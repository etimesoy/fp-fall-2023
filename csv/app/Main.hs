module Main where

import Text.Megaparsec.Error (errorBundlePretty)

import MyLib

main :: IO ()
main = do
  let input = "Name,Age,Gender\nJohn,25,Male\nJane,30,Female\n"
  case parseCSV ',' True input of
      Left err -> putStrLn $ errorBundlePretty err
      Right csv -> print csv
