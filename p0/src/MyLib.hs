module MyLib where

-- Напишите тесты к функции и функцию
--
-- Работает как zip, но если один список
-- длиннее, циклически переиспользует второй
--
-- > zipLong [1,2,3] "abc"
-- [(1,'a'),(2,'b'),(3,'c')]
--
-- > zipLong [1,2] "abcd"
-- [(1,'a'),(2,'b'),(1,'c'),(2,'d')]
--
-- > zipLong [1,2,3] "ab"
-- [(1,'a'),(2,'b'),(3,'a')]
--
-- > zipLong [] "abcd"
-- []
zipLong :: [a] -> [b] -> [(a,b)]
zipLong [] _ = []
zipLong _ [] = []
zipLong as bs = helper 0 as bs where
  as_length = length as
  bs_length = length bs
  helper :: Int -> [a] -> [b] -> [(a,b)]
  helper index as bs
    | index >= as_length && index >= bs_length = []
    | otherwise = (a,b):(helper (index + 1) as bs) where
        a = as!!(index `mod` as_length)
        b = bs!!(index `mod` bs_length)


-- Binary Search Tree
--
-- left < root <= right
data Tree a
  = Empty
  | Node
    { left :: Maybe (Tree a)
    , value :: a
    , right :: Maybe (Tree a)
    }
  deriving (Eq,Show,Read)

empty :: Tree a
empty = Empty

leaf :: a -> Tree a
leaf a = Node Nothing a Nothing

traversal :: Tree a -> [a]
traversal Empty = []
traversal (Node ml v mr)
  = maybe [] traversal ml ++ [v] ++ maybe [] traversal mr

insert :: Ord a => a -> Tree a -> Tree a
insert v Empty = leaf v
insert v t@(Node ml root mr)
  | v < root  = t{ left = Just $ maybe (leaf v) (insert v) ml }
  | otherwise = t{ right= Just $ maybe (leaf v) (insert v) mr }

 -- Напишите тесты-свойства к функциям и сами функции
 -- левого и правого поворота деревьев
 -- (см. https://en.wikipedia.org/wiki/Red%E2%80%93black_tree)
rotateLeft :: Tree a -> Tree a
rotateLeft Empty = Empty
rotateLeft (Node left val (Just (Node rightLeft rightVal rightRight)))
  = Node (Just $ Node left val rightLeft) rightVal rightRight
rotateLeft t = t

rotateRight :: Tree a -> Tree a
rotateRight Empty = Empty
rotateRight (Node (Just (Node leftLeft leftVal leftRight)) val right)
  = Node leftLeft leftVal (Just (Node leftRight val right))
rotateRight t = t
