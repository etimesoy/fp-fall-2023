module Main (main) where

import Test.Tasty
import Test.Tasty.HUnit hiding (assert)
import Test.Tasty.Hedgehog
import Hedgehog
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import Data.List (foldl', sort)

import MyLib

main :: IO ()
main = defaultMain tests

prop_zipLong :: Property
prop_zipLong = property $ do
  xs <- forAll $ Gen.list (Range.linear 0 5) Gen.alpha
  let xs3 = xs ++ xs ++ xs
  zipLong xs3 xs === zip xs3 xs3

tests :: TestTree
tests = testGroup "All tests"
  [ testGroup "zipLong function tests"
    [ testGroup "Unit tests"
      [ testCase "zipLong [] \"abcd\" == []" $ zipLong [] "abcd" @?= ([] :: [(Int, Char)])
      , testCase "zipLong [1,2,3] \"abc\" == [(1,'a'),(2,'b'),(3,'c')]" $
          zipLong [1,2,3] "abc" @?= [(1,'a'),(2,'b'),(3,'c')]
      , testCase "zipLong [1,2] \"abcd\" == [(1,'a'),(2,'b'),(1,'c'),(2,'d')]" $
          zipLong [1,2] "abcd" @?= [(1,'a'),(2,'b'),(1,'c'),(2,'d')]
      , testCase "zipLong [1,2,3] \"ab\" == [(1,'a'),(2,'b'),(3,'a')]" $
          zipLong [1,2,3] "ab" @?= [(1,'a'),(2,'b'),(3,'a')]
      , testCase "zipLong [1,2,3] \"a\" == [(1,'a'),(2,'a'),(3,'a')]" $
          zipLong [1,2,3] "a" @?= [(1,'a'),(2,'a'),(3,'a')]
      , testCase "zipLong \"a\" [1,2,3] == [('a',1),('a',2),('a',3)]" $
          zipLong "a" [1,2,3] @?= [('a',1),('a',2),('a',3)]
      ]
    , testGroup "Property tests"
      [ testProperty "3 concat lists" prop_zipLong
      ]
    ]
  , testGroup "tree rotate tests"
    [ testGroup "rotateLeft function tests"
      [ testCase "empty" $ rotateLeft empty @?= (empty :: Tree Int)
      , testCase "single element" $ rotateLeft (leaf 1) @?= leaf 1
      , testCase "element on left side" $
          rotateLeft (Node (Just $ leaf 1) 2 Nothing) @?= (Node (Just $ leaf 1) 2 Nothing)
      , testCase "elements on both sides of root node" $
          rotateLeft (Node (Just $ leaf 1) 2 (Just $ leaf 3)) @?= (Node (Just $ Node (Just $ leaf 1) 2 Nothing) 3 Nothing)
      , testCase "elements on both sides of right node" $
          rotateLeft (Node Nothing 1 (Just $ Node (Just $ leaf 2) 3 (Just $ leaf 4))) @?= (Node (Just $ Node Nothing 1 (Just $ leaf 2)) 3 (Just $ leaf 4))
      ]
    , testGroup "rotateRight function tests"
      [ testCase "empty" $ rotateRight empty @?= (empty :: Tree Int)
      , testCase "single element" $ rotateRight (leaf 1) @?= leaf 1
      , testCase "element on right side" $
          rotateRight (Node Nothing 1 (Just $ leaf 2)) @?= (Node Nothing 1 (Just $ leaf 2))
      , testCase "elements on both sides of root node" $
          rotateRight (Node (Just $ leaf 1) 2 (Just $ leaf 3)) @?= (Node Nothing 1 (Just $ Node Nothing 2 (Just $ leaf 3)))
      , testCase "elements on both sides of left node" $
          rotateRight (Node (Just $ Node (Just $ leaf 1) 2 (Just $ leaf 3)) 4 Nothing) @?= (Node (Just $ leaf 1) 2 (Just $ Node (Just $ leaf 3) 4 Nothing))
      ]
    , testProperty "arbitraryTree works correct" prop_correct_treeGen
    ]
  ]

-- Поменять генератор двоичных деревьев поиска так,
-- чтобы задавался диапазон элементов дерева и
-- в левом и в правом поддереве генерировались только
-- нужные элементы (чтобы BST было корректным)
arbitraryTree :: Int -> Int -> Size -> Gen (Tree Int)
arbitraryTree _ _ 0 = pure empty
arbitraryTree minVal maxVal size = do
  leftSize <- Size <$> Gen.int (Range.linear 0 $ unSize size - 1)
  let rightSize = size - leftSize - 1
  v <- Gen.int $ Range.linear minVal maxVal
  l <- if leftSize == 0
       then pure Nothing
       else Just <$> arbitraryTree minVal v leftSize
  r <- if rightSize == 0
       then pure Nothing
       else Just <$> arbitraryTree v maxVal rightSize
  pure $ Node l v r

treeGen :: Gen (Tree Int)
treeGen = Gen.sized $ arbitraryTree 0 10000

isCorrect :: Ord a => Tree a -> Bool
isCorrect Empty = True
isCorrect (Node ml v mr) = maybe True isCorrect ml &&
  maybe True isCorrect mr &&
  all (<=v) (maybe [] traversal ml) &&
  all (>=v) (maybe [] traversal mr)

prop_correct_treeGen :: Property
prop_correct_treeGen = property $ do
  t <- forAll treeGen
  assert $ isCorrect t
