{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module MyLib where

import Control.Monad
import Control.Monad.Except
import Control.Monad.Reader
import Control.Monad.State
import Control.Monad.Writer
import System.Directory (doesFileExist, listDirectory, doesDirectoryExist)
import System.Environment
import System.FilePath
import System.IO

data Config = Config
  { depth :: Int, -- Глубина рекурсии
    humanReadable :: Bool, -- Флаг использования человеко-читаемых единиц
    verbose :: Bool -- Флаг вывода отладочной информации
  }

newtype AppState = AppState {currentDepth :: Int} deriving (Eq, Show)

data AppError = FileNotFoundError FilePath | OtherError String deriving (Show)

newtype App a = App
  { unApp :: ReaderT Config (StateT AppState (ExceptT AppError (WriterT [String] IO))) a }
  deriving ( Functor,
             Applicative,
             Monad,
             MonadIO,
             MonadReader Config,
             MonadState AppState,
             MonadError AppError,
             MonadWriter [String]
           )

-- Функция для получения аргументов командной строки
parseArgs :: IO Config
parseArgs = do
  args <- getArgs
  let depthFlag = readDepthFlag args
      humanReadableFlag = "-h" `elem` args
      verboseFlag = "-v" `elem` args
  return $ Config depthFlag humanReadableFlag verboseFlag
  where
    readDepthFlag :: [String] -> Int
    readDepthFlag ("-d" : depthStr : _) = read depthStr
    readDepthFlag ("-s" : _) = 0
    readDepthFlag (_ : rest) = readDepthFlag rest
    readDepthFlag [] = maxBound

-- Функция для получения размера файла
getFileSizeFromPath :: FilePath -> App Integer
getFileSizeFromPath path = do
  exists <- liftIO $ doesFileExist path
  if exists
    then liftIO $ hFileSize =<< openFile path ReadMode
    else throwError $ FileNotFoundError path

-- Функция для обхода директории и подсчета размера
du :: FilePath -> App Integer
du dir = do
  config <- ask
  appState <- get
  let currentDepth' = currentDepth appState
  if currentDepth' > depth config
    then return 0
    else do
      files <- liftIO $ listDirectory dir
      sizes <- forM files $ \file -> do
        let path = dir </> file
        isDir <- liftIO $ doesDirectoryExist path
        if isDir
          then do
            modify (\s -> s { currentDepth = currentDepth' + 1 })
            du path
          else getFileSizeFromPath path
      let totalSize = sum sizes
      when (currentDepth' == 0 && humanReadable config) $
        tell [dir ++ ": " ++ formatSize totalSize]
      when (verbose config) $
        tell ["Depth " ++ show currentDepth' ++ ": " ++ dir ++ ": " ++ show totalSize]
      return totalSize
  where
    formatSize :: Integer -> String
    formatSize size
      | size < 1024 = show size ++ " B"
      | size < 1024 * 1024 = show (size `div` 1024) ++ " KB"
      | size < 1024 * 1024 * 1024 = show (size `div` (1024 * 1024)) ++ " MB"
      | otherwise = show (size `div` (1024 * 1024 * 1024)) ++ " GB"

-- Основная функция
main :: IO ()
main = do
  config <- parseArgs
  let initialState = AppState { currentDepth = 0 }
  (result, logs) <- runWriterT $ runExceptT $ runStateT (runReaderT (unApp (du ".")) config) initialState
  case result of
    Left err -> putStrLn $ "Error: " ++ show err
    Right (totalSize, _) -> do
      putStrLn $ "Total Size: " ++ formatSize totalSize
      putStrLn "Logs:"
      mapM_ putStrLn logs
  where
    formatSize :: Integer -> String
    formatSize size
      | size < 1024 = show size ++ " B"
      | size < 1024 * 1024 = show (size `div` 1024) ++ " KB"
      | size < 1024 * 1024 * 1024 = show (size `div` (1024 * 1024)) ++ " MB"
      | otherwise = show (size `div` (1024 * 1024 * 1024)) ++ " GB"
